﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlideController : MonoBehaviour
{
    public KeyCode NextSlideKey = KeyCode.RightArrow;
    public KeyCode PrevSlideKey = KeyCode.LeftArrow;

    public float SlideDuration = 1.0f;

    public Vector2 FixedVirtualScreenSize = new Vector2(1920, 1080);

    public EasingFunction.Ease EaseFunction;

    private List<GameObject> Slides = new List<GameObject>();

    private int CurrentSlide = -1;
    private int SlideDirection = 0;
    private float TimeSliding = 0.0f;

    // Start is called before the first frame update
    void Start()
    {
        for(int i = 0; i < transform.childCount; ++i)
        {
            GameObject Child = transform.GetChild(i).gameObject;

            Slides.Add(Child);

            if (CurrentSlide == -1 && Child.activeSelf)
            {
                CurrentSlide = i;
            }
            else
            {
                Child.SetActive(false);
            }
        }

        if(CurrentSlide == -1 && Slides.Count > 0)
        {
            CurrentSlide = 0;
            Slides[0].SetActive(true);
        }
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 PrevPosition = new Vector3(0.0f, FixedVirtualScreenSize.y * 1.0f, 0.0f);
        Vector3 CurrentPosition = new Vector3(0.0f, 0.0f, 0.0f);
        Vector3 NextPosition = new Vector3(0.0f, FixedVirtualScreenSize.y * -1.0f, 0.0f);

        if (SlideDirection == 0)
        {
            if (Input.GetKeyDown(NextSlideKey))
            {
                if (CurrentSlide < Slides.Count - 1)
                {
                    SlideDirection = 1;

                    Slides[CurrentSlide + 1].transform.localPosition = NextPosition;
                    Slides[CurrentSlide + 1].SetActive(true);
                }
            }
            if (Input.GetKeyDown(PrevSlideKey))
            {
                if (CurrentSlide > 0)
                {
                    SlideDirection = -1;

                    Slides[CurrentSlide - 1].transform.localPosition = PrevPosition;
                    Slides[CurrentSlide - 1].SetActive(true);
                }
            }
        }
        else
        {
            if(Input.GetKeyDown(NextSlideKey) || Input.GetKeyDown(PrevSlideKey))
            {
                TimeSliding = SlideDuration;
            }

            EasingFunction.Function easingFunction = EasingFunction.GetEasingFunction(EaseFunction);

            TimeSliding += Time.deltaTime;

            float lerpAmount = easingFunction.Invoke(0.0f, 1.0f, TimeSliding / SlideDuration);

            if (TimeSliding > SlideDuration)
            {
                Slides[CurrentSlide].SetActive(false);

                CurrentSlide += SlideDirection;
                TimeSliding = 0.0f;
                SlideDirection = 0;

                Slides[CurrentSlide].transform.localPosition = CurrentPosition;
            }
            else
            {
                if (SlideDirection == 1)
                {
                    Slides[CurrentSlide].transform.localPosition = Vector3.Lerp(CurrentPosition, PrevPosition, lerpAmount);
                    Slides[CurrentSlide + 1].transform.localPosition = Vector3.Lerp(NextPosition, CurrentPosition, lerpAmount);
                }
                if(SlideDirection == -1)
                {
                    Slides[CurrentSlide].transform.localPosition = Vector3.Lerp(CurrentPosition, NextPosition, lerpAmount);
                    Slides[CurrentSlide - 1].transform.localPosition = Vector3.Lerp(PrevPosition, CurrentPosition, lerpAmount);
                }
            }
        }
    }
}
