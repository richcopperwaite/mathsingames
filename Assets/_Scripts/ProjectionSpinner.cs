﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectionSpinner : MonoBehaviour
{
    public VisualVector3D P;
    public VisualVector3D Q;
    public VisualVector3D Projection;
    public LineRenderer Perpendicular;

    public float RotationSpeed = 1.0f;

    public float LineLength = 400.0f;

    public bool ProjectToPoint = false;

    private float _theta = 0.0f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        _theta = Mathf.Repeat(_theta + (Time.deltaTime * RotationSpeed), 360.0f);
        float _thetaRad = _theta * Mathf.Deg2Rad;

        if (!P || !Q || !Projection)
        {
            return;
        }

        Vector3 vector = new Vector3(Mathf.Sin(_thetaRad), Mathf.Cos(_thetaRad), 0.0f) * LineLength;

        Q.FollowWorldSpaceVector = false;
        Q.transform.localRotation = Quaternion.Euler(0.0f, 0.0f, -_theta);

        Vector3 projVec = Vector3.Project(Vector3.up * LineLength, vector);

        Projection.FollowWorldSpaceVector = false;
        Projection.SetVector(projVec);

        if(ProjectToPoint)
        {
            Projection.transform.localPosition = new Vector3(0.0f, LineLength, 0.0f) - projVec;
        }

        if (Perpendicular)
        {
            Perpendicular.SetPositions(new Vector3[] { Vector3.up * LineLength, projVec });
        }
    }
}
