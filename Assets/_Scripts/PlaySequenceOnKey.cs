﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class PlaySequenceOnKey : MonoBehaviour
{
    public KeyCode KeyToPress = KeyCode.D;

    public PlayableDirector[] DirectorComponents;

    int _CurrentPlayable = 0;

    private void OnEnable()
    {
        //if (AnimatorComponent != null)
        //{
        //    AnimatorComponent.ResetTrigger(AnimatorTrigger);
        //    AnimatorComponent.SetTrigger(ResetTrigger);
        //}
        if(DirectorComponents != null)
        {
            foreach (PlayableDirector DirectorComponent in DirectorComponents)
            {
                DirectorComponent.Stop();
            }
        }

        _CurrentPlayable = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyToPress))
        {
            //if(AnimatorComponent != null)
            //{
            //    AnimatorComponent.ResetTrigger(ResetTrigger);
            //    AnimatorComponent.SetTrigger(AnimatorTrigger);
            //}
            if (DirectorComponents != null && _CurrentPlayable < DirectorComponents.Length)
            {
                DirectorComponents[_CurrentPlayable].Play();
                _CurrentPlayable++;
            }
        }
    }
}
