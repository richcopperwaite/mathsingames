﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrossProductAnimator : MonoBehaviour
{
    public VisualVector3D Line1;
    public VisualVector3D Line2;
    public VisualVector3D Line3;

    public float RotationSpeed = 1.0f;

    private float _theta = 0.0f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!Line1 || !Line2 || !Line3)
            return;

        _theta += Time.deltaTime * 360.0f * RotationSpeed;
        _theta = Mathf.Repeat(_theta, 360.0f);

        Line2.transform.localRotation = Quaternion.Euler(Vector3.up * _theta);

        Vector3 CrossProduct = Vector3.Cross(Line1.transform.forward, Line2.transform.forward);

        //Debug.Log(CrossProduct);

        Line3.SetVector(CrossProduct, 1.5f);
    }
}
