﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AircraftControls : MonoBehaviour
{
    public KeyCode YawLeft = KeyCode.A;
    public KeyCode YawRight = KeyCode.D;
    public KeyCode PitchUp = KeyCode.W;
    public KeyCode PitchDown = KeyCode.S;
    public KeyCode RollLeft = KeyCode.Q;
    public KeyCode RollRight = KeyCode.E;

    public float RotationSpeed = 1.0f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    protected void Update()
    {
        float rotationAmount = Time.deltaTime * RotationSpeed;

        if(Input.GetKey(YawLeft))
        {
            transform.localRotation *= Quaternion.Euler(0.0f, -rotationAmount, 0.0f);
        }
        if (Input.GetKey(YawRight))
        {
            transform.localRotation *= Quaternion.Euler(0.0f, rotationAmount, 0.0f);
        }
        if (Input.GetKey(PitchUp))
        {
            transform.localRotation *= Quaternion.Euler(-rotationAmount, 0.0f, 0.0f);
        }
        if (Input.GetKey(PitchDown))
        {
            transform.localRotation *= Quaternion.Euler(rotationAmount, 0.0f, 0.0f);
        }
        if (Input.GetKey(RollLeft))
        {
            transform.localRotation *= Quaternion.Euler(0.0f, 0.0f, rotationAmount);
        }
        if (Input.GetKey(RollRight))
        {
            transform.localRotation *= Quaternion.Euler(0.0f, 0.0f, -rotationAmount);
        }
    }
}
