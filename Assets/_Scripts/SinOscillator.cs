﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SinOscillator : MonoBehaviour
{
    public float AngleRange = 60.0f;

    public float Speed = 1.0f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.localRotation = Quaternion.Euler(Vector3.forward * Mathf.Sin(Time.timeSinceLevelLoad * Speed) * AngleRange * 0.5f);
    }
}
