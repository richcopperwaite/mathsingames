﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightingSceneAnimator : MonoBehaviour
{
    public KeyCode KeyToPress = KeyCode.D;

    public GameObject Vertices;
    public GameObject Normals;
    public GameObject LightArrow;
    public MeshRenderer Cube;

    public string WireframeMatColor = "_Color";
    public int WireframeMatIndex = 0;
    public string QuadMatColor = "_Color";
    public int QuadMatIndex = 0;
    public string LitMatColor = "_Color";
    public int LitMatIndex = 0;

    public float TransitionDuration = 2.0f;

    private int CurrentState = 0;
    private bool Transitioning = false;

    // Start is called before the first frame update
    void OnEnable()
    {
        CurrentState = 0;

        Vertices.SetActive(true);
        Normals.SetActive(false);
        LightArrow.SetActive(false);

        ResetMaterial(WireframeMatIndex, WireframeMatColor);
        ResetMaterial(QuadMatIndex, QuadMatColor);
        ResetMaterial(LitMatIndex, LitMatColor);
    }

    void ResetMaterial(int MaterialIndex, string Property)
    {
        Color cachedColor = Cube.materials[MaterialIndex].GetColor(Property);
        cachedColor.a = 0.0f;
        Cube.materials[MaterialIndex].SetColor(Property, cachedColor);
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyToPress) && !Transitioning)
        {
            switch(CurrentState)
            {
                case 0:
                    StartCoroutine(SetEnabledAfter(Vertices, false));
                    StartCoroutine(FadeColor(WireframeMatIndex, WireframeMatColor, true));
                    break;
                case 1:
                    StartCoroutine(FadeColor(WireframeMatIndex, WireframeMatColor, false));
                    StartCoroutine(FadeColor(QuadMatIndex, QuadMatColor, true));
                    break;
                case 2:
                    Normals.SetActive(true);
                    break;
                case 3:
                    LightArrow.SetActive(true);
                    break;
                case 4:
                    StartCoroutine(FadeColor(QuadMatIndex, QuadMatColor, false));
                    StartCoroutine(FadeColor(LitMatIndex, LitMatColor, true));
                    break;
                case 5:
                    Normals.SetActive(false);
                    break;
            }

            ++CurrentState;
        }
    }

    private IEnumerator FadeColor(int MaterialIndex, string Property, bool In)
    {
        Transitioning = true;

        float TimeInState = 0.0f;

        Color cachedColor = Cube.materials[MaterialIndex].GetColor(Property);
        Color targetColor = cachedColor;
        targetColor.a = In ? 1.0f : 0.0f;

        while (TimeInState < TransitionDuration)
        {
            TimeInState += Time.deltaTime;

            Cube.materials[MaterialIndex].SetColor(Property, Color.Lerp(cachedColor, targetColor, TimeInState / TransitionDuration) );

            yield return null;
        }

        Transitioning = false;

        yield return null;
    }

    private IEnumerator SetEnabledAfter(GameObject Target, bool Enabled)
    {
        yield return new WaitForSeconds(TransitionDuration);

        Target.SetActive(Enabled);
    }
}
