﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleSpin : MonoBehaviour
{
    public Vector3 SpinSpeed;

    // Start is called before the first frame update
    //void Start()
    //{
        
    //}

    // Update is called once per frame
    void Update()
    {
        Quaternion currentRotation = transform.localRotation;
        currentRotation.eulerAngles += SpinSpeed * Time.deltaTime;
        transform.localRotation = currentRotation;
    }
}
