﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceshipControls : AircraftControls
{
    public KeyCode StrafeLeft = KeyCode.J;
    public KeyCode StrafeRight = KeyCode.L;
    public KeyCode MoveForward = KeyCode.I;
    public KeyCode MoveBackward = KeyCode.K;
    public KeyCode MoveUpward = KeyCode.Space;
    public KeyCode MoveDownward = KeyCode.LeftControl;

    public float MovementSpeed = 1.0f;

    public VisualVector3D CrossVector;

    public Vector3 Forward;
    public Vector3 Up;
    public Vector3 Right;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        base.Update();
        
        float movementAmount = Time.deltaTime * MovementSpeed;

        if(Input.GetKey(StrafeLeft))
        {
            transform.position += transform.TransformVector(Right) * -movementAmount;
            CrossVector.transform.localRotation = Quaternion.Euler(Vector3.up * -90.0f);
            CrossVector.gameObject.SetActive(true);
        }
        else if (Input.GetKey(StrafeRight))
        {
            transform.position += transform.TransformVector(Right) * movementAmount;
            CrossVector.transform.localRotation = Quaternion.Euler(Vector3.up * 90.0f);
            CrossVector.gameObject.SetActive(true);
        }
        else
        {
            CrossVector.gameObject.SetActive(false);
        }

        if (Input.GetKey(MoveForward))
        {
            transform.position += transform.TransformVector(Forward) * movementAmount;
        }
        if (Input.GetKey(MoveBackward))
        {
            transform.position += transform.TransformVector(Forward) * -movementAmount;
        }
        if (Input.GetKey(MoveUpward))
        {
            transform.position += transform.TransformVector(Up) * movementAmount;
        }
        if (Input.GetKey(MoveDownward))
        {
            transform.position += transform.TransformVector(Up) * -movementAmount;
        }
    }
}
