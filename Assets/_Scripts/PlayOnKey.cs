﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayOnKey : MonoBehaviour
{
    public KeyCode KeyToPress = KeyCode.D;

    public Animator AnimatorComponent;
    public string AnimatorTrigger;
    public string ResetTrigger;

    public UnityEngine.Playables.PlayableDirector DirectorComponent;

    private void OnEnable()
    {
        if (AnimatorComponent != null)
        {
            AnimatorComponent.ResetTrigger(AnimatorTrigger);
            AnimatorComponent.SetTrigger(ResetTrigger);
        }
        if(DirectorComponent != null)
        {
            DirectorComponent.Stop();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyToPress))
        {
            if(AnimatorComponent != null)
            {
                AnimatorComponent.ResetTrigger(ResetTrigger);
                AnimatorComponent.SetTrigger(AnimatorTrigger);
            }
            if (DirectorComponent != null)
            {
                DirectorComponent.Play();
            }
        }
    }
}
