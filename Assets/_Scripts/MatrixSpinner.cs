﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MatrixSpinner : MonoBehaviour
{
    public float MinScale = 0.5f;
    public float MaxScale = 1.25f;

    public float MinScaleDelta = 0.25f;

    public float MinRotation = -180.0f;
    public float MaxRotation = 180.0f;

    public float MinRotationDelta = 30.0f;

    public float SpinDuration = 1.0f;
    public float SpinInterval = 1.0f;

    private float prevScale = 1.0f;
    private float newScale = 1.0f;

    private float prevRotation = 0.0f;
    private float newRotation = 0.0f;

    private bool _spinning = false;
    private float _timeInState = 0.0f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        _timeInState += Time.deltaTime;

        if (_spinning)
        {
            if(_timeInState > SpinDuration)
            {
                transform.localScale = Vector3.one * newScale;
                transform.localRotation = Quaternion.Euler(Vector3.forward * newRotation);

                _timeInState = 0.0f;
                _spinning = false;
            }
            else
            {
                transform.localScale = Vector3.one * Mathf.Lerp(prevScale, newScale, _timeInState / SpinDuration);
                transform.localRotation = Quaternion.Euler(Vector3.forward * Mathf.Lerp(prevRotation, newRotation, _timeInState / SpinDuration));
            }
        }
        else
        {
            if(_timeInState > SpinInterval)
            {
                prevScale = newScale;
                prevRotation = newRotation;

                int attempts = 0;
                while (Mathf.Abs(newScale - prevScale) < MinScaleDelta && attempts++ < 5)
                {
                    newScale = Random.Range(MinScale, MaxScale);
                }
                attempts = 0;
                while(Mathf.Abs(newRotation - prevRotation) < MinRotationDelta && attempts++ < 5)
                {
                    newRotation = Random.Range(MinRotation, MaxRotation);
                }

                _timeInState = 0.0f;
                _spinning = true;
            }
        }
    }
}
