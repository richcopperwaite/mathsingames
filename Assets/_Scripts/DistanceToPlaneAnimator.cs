﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class DistanceToPlaneAnimator : MonoBehaviour
{
    public Transform PlanePivot;

    public float PlaneRotationSpeed = 60.0f;

    public GameObject Point;

    public Vector3 PointRange;
    public Vector3 PointSpeed;

    public LineRenderer DistanceLine;
    public LineRenderer ReferenceLine;
    public Transform NormalLine;

    private float _PlaneTheta = 0.0f;
    private Vector3 _PointThetas = Vector3.zero;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        _PlaneTheta = Mathf.Repeat(_PlaneTheta + (Time.deltaTime * PlaneRotationSpeed), 360.0f);

        if (!PlanePivot) return;

        PlanePivot.transform.localRotation = Quaternion.Euler(Vector3.forward * _PlaneTheta);

        _PointThetas += Time.deltaTime * PointSpeed;

        Vector3 PointThetasRads = _PointThetas * Mathf.Deg2Rad;

        Vector3 PointOffset = new Vector3(Mathf.Sin(PointThetasRads.x), Mathf.Sin(PointThetasRads.y), Mathf.Sin(PointThetasRads.z));

        if (!Point) return;

        Point.transform.localPosition = Vector3.Scale(PointOffset, PointRange);

        if (!NormalLine) return;

        Vector3 Projection = Vector3.Project(Point.transform.position - NormalLine.transform.position, NormalLine.forward);

        if (!DistanceLine) return;

        DistanceLine.SetPositions(new Vector3[] { Point.transform.position, Point.transform.position - Projection });

        if (!ReferenceLine) return;

        ReferenceLine.SetPositions(new Vector3[] { Point.transform.position, NormalLine.transform.position });
    }
}
