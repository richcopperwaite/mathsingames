﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshRenderer))]
public class CycleTextureOffset : MonoBehaviour
{
    private MeshRenderer _meshRenderer;

    public Vector2 ScrollSpeed;
    
    // Start is called before the first frame update
    void Start()
    {
        _meshRenderer = GetComponent<MeshRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 CurrentTextureOffset = _meshRenderer.material.mainTextureOffset;
        CurrentTextureOffset += (ScrollSpeed * Time.deltaTime);
        CurrentTextureOffset.x = Mathf.Repeat(CurrentTextureOffset.x, 1.0f);
        CurrentTextureOffset.y = Mathf.Repeat(CurrentTextureOffset.y, 1.0f);
        _meshRenderer.material.mainTextureOffset = CurrentTextureOffset;
    }
}
