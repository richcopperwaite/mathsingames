﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class CirclePlotter : MonoBehaviour
{
    public LineRenderer xLine;
    public LineRenderer yLine;
    public LineRenderer hypLine;
    public LineRenderer circleLine;

    public float Radius = 350;

    public float YRadius = 350;

    public float DegreesPerSecond = 360.0f;

    public int Samples = 60;

    private float _theta = 0.0f;

    private List<Vector3> _circlePoints = new List<Vector3>();

    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void OnEnable()
    {
        _circlePoints.Clear();
    }

    // Update is called once per frame
    void Update()
    {
        _theta += Time.deltaTime * DegreesPerSecond;

        float x = Radius * Mathf.Sin(_theta * Mathf.Deg2Rad);
        float y = YRadius * Mathf.Cos(_theta * Mathf.Deg2Rad);

        if (xLine)
        {
            xLine.SetPositions(new Vector3[] { Vector3.zero, Vector3.right * x });
        }
        if (yLine)
        {
            yLine.SetPositions(new Vector3[] { Vector3.right * x, new Vector3(x, y, 0.0f) });
        }
        if(hypLine)
        {
            hypLine.SetPositions(new Vector3[] { Vector3.zero, new Vector3(x, y, 0.0f) });
        }
        if(_circlePoints.Count != Samples)
        {
            _circlePoints.Clear();
            for(int i = 0; i < Samples; ++i)
            {
                float preComputedX = Radius * Mathf.Sin(Mathf.PI * 2.0f * ((float)i / (Samples - 1)));
                float preComputedY = YRadius * Mathf.Cos(Mathf.PI * 2.0f * ((float)i / (Samples - 1)));
                _circlePoints.Add(new Vector3(preComputedX, preComputedY, 0.0f));
            }
        }
        if(circleLine)
        {
            int count = (int)Mathf.Floor(((float)Samples) * Mathf.Repeat(_theta / 360.0f, 1.0f));
            circleLine.positionCount = count;
            if (count <= _circlePoints.Count)
            {
                circleLine.SetPositions(_circlePoints.GetRange(0, count).ToArray());
            }
            //circleLine.positionCount = _circlePoints.Count;
            //circleLine.SetPositions(_circlePoints.ToArray());
        }
    }
}
