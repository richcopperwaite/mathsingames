﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class PropagateActivation : MonoBehaviour
{
    public GameObject[] SubObjects;
    
    void OnEnable()
    {
        if (SubObjects == null) return;

        foreach(GameObject go in SubObjects)
        {
            if (go == null) continue;
            go.SetActive(true);
        }
    }

    private void OnDisable()
    {
        if (SubObjects == null) return;

        foreach (GameObject go in SubObjects)
        {
            if (go == null) continue;
            go.SetActive(false);
        }
    }
}
