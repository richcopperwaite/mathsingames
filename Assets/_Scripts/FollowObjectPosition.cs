﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class FollowObjectPosition : MonoBehaviour
{
    public Transform ObjectToFollow;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!ObjectToFollow)
            return;

        transform.position = ObjectToFollow.position;
    }
}
