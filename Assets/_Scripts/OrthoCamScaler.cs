﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
[ExecuteInEditMode]
public class OrthoCamScaler : MonoBehaviour
{
    private Camera _camera;

    public float Depth = -10.0f;

    // Start is called before the first frame update
    void Start()
    {
        _camera = GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        _camera.transform.position = new Vector3(Screen.width / 2, Screen.height / 2, Depth);

        _camera.orthographicSize = Screen.height / 2;
    }
}
