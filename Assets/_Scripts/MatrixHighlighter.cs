﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MatrixHighlight
{
    public GameObject HighlightObject;
    public Vector3[] Positions;

    [HideInInspector]
    public int CurrentPosition;

    public int NextPosition
    {
        get { return (CurrentPosition + 1) % Positions.Length; }
    }

    public Vector3 Lerp(float t)
    {
        return Vector3.Lerp(Positions[CurrentPosition], Positions[NextPosition], t);
    }
}

public class MatrixHighlighter : MonoBehaviour
{
    public MatrixHighlight[] Highlights;

    public float SwitchDuration;
    public float SwitchInterval;

    public EasingFunction.Ease Ease;

    private bool switching = false;
    private float timeInState = 0.0f;

    // Update is called once per frame
    void Update()
    {
        timeInState += Time.deltaTime;

        EasingFunction.Function easeFunction = EasingFunction.GetEasingFunction(Ease);

        if(switching)
        {
            if(timeInState > SwitchDuration)
            {
                for(int i = 0; i < Highlights.Length; ++i)
                {
                    MatrixHighlight highlight = Highlights[i];
                    highlight.HighlightObject.transform.localPosition = highlight.Positions[highlight.NextPosition];
                    highlight.CurrentPosition = highlight.NextPosition;
                }

                timeInState = 0.0f;
                switching = false;
            }
            else
            {
                for (int i = 0; i < Highlights.Length; ++i)
                {
                    MatrixHighlight highlight = Highlights[i];
                    highlight.HighlightObject.transform.localPosition = highlight.Lerp(easeFunction.Invoke(0.0f, 1.0f, (timeInState / SwitchDuration)));
                }
            }
        }
        else
        {
            if(timeInState > SwitchInterval)
            {
                timeInState = 0.0f;
                switching = true;
            }
        }
    }
}
