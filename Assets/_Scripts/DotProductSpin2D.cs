﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DotProductSpin2D : MonoBehaviour
{
    public LineRenderer Line1;
    public LineRenderer Line2;
    public LineRenderer DotVisLine;

    private Vector2 _vec1 = Vector2.up;
    private Vector2 _vec2 = Vector2.up;

    public float Speed = 1.0f;
    public float LineLength = 200;

    public Vector3 DotVisLineRange;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        _vec1 = new Vector2(Mathf.Sin(Time.timeSinceLevelLoad * Speed * 0.2f), Mathf.Cos(Time.timeSinceLevelLoad * Speed * 0.2f));

        if(Line1)
        {
            Line1.SetPositions(new Vector3[] { Vector3.zero, new Vector3(_vec1.x, _vec1.y, 0.0f) * LineLength });
        }

        _vec2 = new Vector2(Mathf.Sin(Time.timeSinceLevelLoad * Speed), Mathf.Cos(Time.timeSinceLevelLoad * Speed));

        if (Line2)
        {
            Line2.SetPositions(new Vector3[] { Vector3.zero, new Vector3(_vec2.x, _vec2.y, 0.0f) * LineLength });
        }

        float DotProduct = Vector2.Dot(_vec1, _vec2);

        if (DotVisLine == null) return;

        DotVisLine.SetPositions(new Vector3[] { Vector3.zero, DotVisLineRange * DotProduct });
    }
}
