﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DotProductControl3D : MonoBehaviour
{
    public VisualVector3D VisualVector1;
    public VisualVector3D VisualVector2;

    public Transform TargetPoint;

    public LineRenderer ReferenceHorizLine;
    public LineRenderer ReferenceVertLine;

    public Transform Panel;

    public KeyCode YawLeft;
    public KeyCode YawRight;
    public KeyCode PitchUp;
    public KeyCode PitchDown;

    public float RotationSpeed = 120.0f;

    Vector3 vecRotation;

    // Start is called before the first frame update
    void OnEnable()
    {
        vecRotation = new Vector3(0.0f, 90.0f, 0.0f);
        VisualVector2.transform.localRotation = Quaternion.Euler(vecRotation);
    }

    // Update is called once per frame
    void Update()
    {
        Quaternion rotation = Quaternion.identity;

        if(Input.GetKey(YawLeft))
        {
            vecRotation += Vector3.up * -RotationSpeed * Time.deltaTime;
        }
        if(Input.GetKey(YawRight))
        {
            vecRotation += Vector3.up * RotationSpeed * Time.deltaTime;
        }
        if(Input.GetKey(PitchUp))
        {
            float pitchValue = Mathf.Clamp(vecRotation.x + (-RotationSpeed * Time.deltaTime), -90.0f, 90.0f);
            vecRotation = new Vector3(pitchValue, vecRotation.y, vecRotation.z);
        }
        if(Input.GetKey(PitchDown))
        {
            float pitchValue = Mathf.Clamp(vecRotation.x + (RotationSpeed * Time.deltaTime), -90.0f, 90.0f);
            vecRotation = new Vector3(pitchValue, vecRotation.y, vecRotation.z);
        }

        VisualVector2.transform.localRotation = Quaternion.Euler(vecRotation);

        Panel.localScale = new Vector3(Vector3.Dot(VisualVector1.transform.forward, VisualVector2.transform.forward), 1.0f, 1.0f);

        ReferenceHorizLine.transform.localRotation = Quaternion.Euler(0.0f, vecRotation.y, 0.0f);
        ReferenceHorizLine.transform.localScale = new Vector3(1.0f, 1.0f, Mathf.Cos(Mathf.Deg2Rad * vecRotation.x));

        ReferenceVertLine.SetPositions(new Vector3[]
        {
            TargetPoint.position,
            new Vector3(TargetPoint.position.x, 0.0f, TargetPoint.position.z)
        });
    }
}
