﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class GraphAnimator : MonoBehaviour
{
    public LineRenderer PointToX;
    public LineRenderer PointToY;
    public LineRenderer FunctionLine;
    public Transform GraphPosition;
    public Transform GraphPoint;

    public Transform ExamplePoint;
    public Transform ExamplePoint2;

    public int Samples = 30;

    public EasingFunction.Ease EasingFunc;

    public Vector3 GraphSize = Vector3.one;
    public Vector3 ExamplePointRange = Vector3.one;
    public Vector3 ExamplePoint2Range = Vector3.one;

    public float Speed = 1.0f;

    float _progress = 0.0f;

    Vector3[] _positions;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // Create the array of points along the curve
        if(_positions == null || _positions.Length != Samples + 1)
        {
            _positions = new Vector3[Samples];
        }

        for(int i = 0; i < Samples; ++i)
        {
            float sampleProgress = i / (float)(Samples - 1);

            float sampleValue = EasingFunction.GetEasingFunction(EasingFunc)(0.0f, 1.0f, sampleProgress);

            _positions[i] = new Vector3(GraphSize.x * sampleProgress, GraphSize.y * sampleValue, GraphSize.z);
        }

        if (FunctionLine != null)
        {
            if (FunctionLine.positionCount != Samples)
            {
                FunctionLine.positionCount = Samples;
            }

            FunctionLine.SetPositions(_positions);
        }

        _progress = Mathf.PingPong(Time.timeSinceLevelLoad * Speed, 1.0f);

        float pointValue = EasingFunction.GetEasingFunction(EasingFunc)(0.0f, 1.0f, _progress);

        if (GraphPoint != null)
        {
            GraphPoint.transform.localPosition = new Vector3(GraphSize.x * _progress, GraphSize.y * pointValue, GraphSize.z);
        }

        if (PointToX != null)
        {
            PointToX.SetPositions(new Vector3[]
            {
                new Vector3(GraphSize.x * _progress, 0.0f, 0.0f),
                new Vector3(GraphSize.x * _progress, GraphSize.y * pointValue, 0.0f)
            });
        }

        if (PointToY != null)
        {
            PointToY.SetPositions(new Vector3[]
            {
                new Vector3(0.0f, GraphSize.y * pointValue, 0.0f),
                new Vector3(GraphSize.x * _progress, GraphSize.y * pointValue, 0.0f)
            });
        }

        if(ExamplePoint != null)
        {
            ExamplePoint.transform.localPosition = ExamplePointRange * pointValue;
        }
        if (ExamplePoint2 != null)
        {
            ExamplePoint2.transform.localPosition = ExamplePoint2Range * pointValue;
        }
    }
}
