﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class VisualVector3D : MonoBehaviour
{
    public LineRenderer Renderer;

    [Min(0.0f)]
    public float Length = 1.0f;

    [Min(0.0f)]
    public float Width = 1.0f;

    [Min(0.0f)]
    public float ArrowheadStretch = 1.0f;

    public float AlignmentOffset = 0.0f;

    public bool FollowWorldSpaceVector = false;

    public Vector3 WorldSpaceVector = Vector3.forward;

    private Vector3 VectorBasis = Vector3.forward;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Renderer == null) return;

        float ArrowheadLength = Mathf.Min(Length, ArrowheadStretch);
        
        Renderer.SetPositions(new Vector3[] {
            VectorBasis * Length * -AlignmentOffset,
            VectorBasis * ((Length * (1.0f-AlignmentOffset)) - ArrowheadLength),
            VectorBasis * Length * (1.0f-AlignmentOffset) });

        Renderer.widthCurve = AnimationCurve.Constant(0.0f, 1.0f, Width);

        if(FollowWorldSpaceVector)
        {
            transform.rotation = Quaternion.LookRotation(WorldSpaceVector, Vector3.up);
        }
    }

    public void SetVector(Vector3 vector, float scaleFactor = 1.0f)
    {
        VectorBasis = vector.normalized;

        Length = vector.magnitude * scaleFactor;
    }
}
