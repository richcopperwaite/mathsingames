﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceSceneController : MonoBehaviour
{
    public TMPro.TextMeshPro TextMesh;

    public Transform ObjectiveDirection;
    public Transform PlayerDirection;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(TextMesh == null || ObjectiveDirection == null || PlayerDirection == null)
        {
            Debug.LogError("Can't update Wrong Way Text - a property is null!");
        }

        float DotProduct = Vector3.Dot(ObjectiveDirection.forward, PlayerDirection.forward);
        
        TextMesh.gameObject.SetActive(DotProduct < 0.0f && Mathf.Repeat(Time.timeSinceLevelLoad, 1.0f) > 0.5f);
    }
}
