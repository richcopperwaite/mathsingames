﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DistanceToLineAnimator : MonoBehaviour
{
    public Vector3 LinePoint;
    public Vector3 LineNormal;
    public Vector3 Point;

    // Start is called before the first frame update
    void Start()
    {
        LineNormal = LineNormal.normalized;
        Vector3 Offset = LinePoint - Point;
        float DotProduct = Vector3.Dot(Offset, LineNormal);
        float OfficialDistance = (Offset - (DotProduct * LineNormal)).magnitude;
        float SuspectedDistance = (DotProduct * LineNormal).magnitude;

        Debug.Log("OfficialDistance: " + OfficialDistance + "   SuspectedDistance: " + SuspectedDistance);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
