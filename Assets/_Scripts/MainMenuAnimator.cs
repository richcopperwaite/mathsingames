﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuAnimator : MonoBehaviour
{
    public Transform[] MainMenuElements;
    private Vector3[] ElementOriginalPositions;

    public EasingFunction.Ease[] EasesToTry;

    public KeyCode PreviousEase;
    public KeyCode RepeatEase;
    public KeyCode NextEase;

    public float OffscreenDistance = 1920;
    public float EaseDuration = 1.0f;
    public float ElementStagger = 1.0f;

    //private float TimeInEase = 0.0f;
    private int CurrentEase = 0;
    private int ElementsAnimating = 0;

    // Start is called before the first frame update
    void Start()
    {
    }

    private void OnEnable()
    {
        CurrentEase = 0;

        ElementOriginalPositions = new Vector3[MainMenuElements.Length];

        for (int i = 0; i < MainMenuElements.Length; ++i)
        {
            ElementOriginalPositions[i] = MainMenuElements[i].transform.localPosition;
        }

        RestartAnimation();
    }

    private void OnDisable()
    {
        for (int i = 0; i < MainMenuElements.Length; ++i)
        {
            MainMenuElements[i].transform.localPosition = ElementOriginalPositions[i];
        }
    }

    private void RestartAnimation()
    {
        for(int i = 0; i < MainMenuElements.Length; ++i)
        {
            StartCoroutine(AnimateElementCoroutine(i));
        }
        ElementsAnimating = MainMenuElements.Length;
    }

    private IEnumerator AnimateElementCoroutine(int element)
    {
        Vector3 ElementEndPosition = MainMenuElements[element].transform.localPosition;
        Vector3 ElementStartPosition = ElementEndPosition + (Vector3.right * (OffscreenDistance * (element % 2 == 0 ? 1 : -1)));
        EasingFunction.Function EaseFunction = EasingFunction.GetEasingFunction(EasesToTry[CurrentEase]);

        MainMenuElements[element].transform.localPosition = ElementStartPosition;

        yield return new WaitForSeconds(element * ElementStagger);

        float ElementStartTime = Time.timeSinceLevelLoad;
        while(Time.timeSinceLevelLoad < ElementStartTime + EaseDuration)
        {
            float LerpValue = EaseFunction.Invoke(0.0f, 1.0f, (Time.timeSinceLevelLoad - ElementStartTime) / EaseDuration);
            MainMenuElements[element].transform.localPosition = Vector3.Lerp(ElementStartPosition, ElementEndPosition, LerpValue);
            yield return null;
        }

        MainMenuElements[element].transform.localPosition = ElementEndPosition;

        --ElementsAnimating;
    }

    // Update is called once per frame
    void Update()
    {
        int RequestedEase = -1;

        if (ElementsAnimating == 0)
        {
            if (Input.GetKeyDown(PreviousEase) && CurrentEase > 0)
            {
                RequestedEase = CurrentEase - 1;
            }
            if (Input.GetKeyDown(RepeatEase))
            {
                RequestedEase = CurrentEase;
            }
            if (Input.GetKeyDown(NextEase) && CurrentEase < EasesToTry.Length - 1)
            {
                RequestedEase = CurrentEase + 1;
            }
        }

        if(RequestedEase != -1)
        {
            CurrentEase = RequestedEase;
            RestartAnimation();
        }
    }
}
