﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class DisableIfOpposing : MonoBehaviour
{
    public Transform Transform1;
    public Transform Transform2;
    public GameObject ObjectToDisable;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (ObjectToDisable == null || Transform1 == null || Transform2 == null)
            return;

        ObjectToDisable.SetActive(Vector3.Dot(Transform1.forward, Transform2.forward) < 0.0f);
    }
}
